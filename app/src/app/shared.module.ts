import { ReactiveFormsModule } from '@angular/forms';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading/loading.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [LoadingComponent],
  imports: [MatProgressSpinnerModule, CommonModule, ReactiveFormsModule, MatButtonModule],
  exports: [LoadingComponent, ReactiveFormsModule, MatButtonModule],
})
export class SharedModule {}
