import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Heroes } from '../heroes/heroes';

@Injectable({
  providedIn: 'root',
})
export class CrudService {
  backedUrl: string = 'http://localhost:3000/heroes';

  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) {}

  /**
   * Lanza la petición para obtener los héroes, si recibe un id obtendrá el héroe con ese id,
   *  si no todos
   */
  get(id?: number): Observable<any> {
    const requestUrl = id === undefined ? `${this.backedUrl}` : `${this.backedUrl}/${id}`;
    return this.httpClient.get(requestUrl, { headers: this.httpHeaders }).pipe(
      map((res: any) => {
        return res || {};
      }),
      catchError(this.handleError),
    );
  }

  /**
   * Lanza la petición POST para crear los héroes
   */
  create(data: any): Observable<any> {
    const requestUrl = `${this.backedUrl}`;
    return this.httpClient
      .post(requestUrl, data, { headers: this.httpHeaders })
      .pipe(catchError(this.handleError));
  }

  /**
   * Lanza la petición PUT para actualizar el héroe del id recibido con los datos recibidos
   */
  update(id: any, data: any): Observable<any> {
    const requestUrl = `${this.backedUrl}/${id}`;
    return this.httpClient
      .put(requestUrl, data, { headers: this.httpHeaders })
      .pipe(catchError(this.handleError));
  }

  /**
   * Lanza la petición PUT para eliminar el héroe del id recibido
   */
  delete(id: number): Observable<any> {
    const requestUrl = `${this.backedUrl}/${id}`;
    return this.httpClient
      .delete(requestUrl, { headers: this.httpHeaders })
      .pipe(catchError(this.handleError));
  }

  /**
   * En caso de que se produzca algún error con las peticiones lo captura y muestra por consola
   */
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }

  /**
   * Filtra el array de héroes recibido devolviendo todos los que en el nombre contengan
   * la cadena recibida
   */
  filterHeroes(heroes: Heroes[], filterInput: string): Heroes[] {
    const filteredHeroes = heroes.filter((heroe) => {
      return heroe.name.indexOf(filterInput) !== -1;
    });

    return filteredHeroes;
  }
}
