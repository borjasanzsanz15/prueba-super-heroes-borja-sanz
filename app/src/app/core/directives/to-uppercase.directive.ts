import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appToUpperCase]',
})
export class ToUpperCaseDirective {
  /**
   * En cada input recoge el valor del campo y lo guarda en mayúsculas
   */
  @HostListener('input', ['$event']) onInput(event: any) {
    event.target['value'] = event.target['value'].toUpperCase();
  }
}
