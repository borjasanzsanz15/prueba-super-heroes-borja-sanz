import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoadingService } from '../loading/service/loading.service';
import { catchError, finalize } from 'rxjs/operators';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  constructor(private loadingService: LoadingService) {}

  /**
   * Intercepta todas las peticiones, cuando se inician llama a loadingService para que la añada al array
   * cuando finaliza para que la quite del array.
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loadingService.setLoadingPage(req.url);
    return next.handle(req).pipe(finalize(() => this.loadingService.endLoadingPage(req.url)));
  }
}
