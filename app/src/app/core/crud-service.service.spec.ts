import { Heroes } from './../heroes/heroes';
import { TestBed } from '@angular/core/testing';
import { CrudService } from './crud-service.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule, HttpRequest } from '@angular/common/http';

describe('CrudService', () => {
  let crudService: CrudService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      providers: [CrudService],
    });
    crudService = TestBed.inject(CrudService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(crudService).toBeTruthy();
  });

  it('Crear un nuevo heroe', () => {
    const mockHero = {
      name: 'spiderman',
    };

    crudService.create({ name: 'spiderman' }).subscribe((heroes) => {
      expect(heroes.name).toEqual('spiderman');
    });

    const req = httpTestingController.expectOne('http://localhost:3000/heroes');

    expect(req.request.method).toEqual('POST');

    req.flush(mockHero);
  });

  it('Editar un nuevo heroe', () => {
    const mockHero = {
      name: 'spiderman',
    };

    crudService.update(11, { name: 'spiderman' }).subscribe((heroes) => {
      expect(heroes.name).toEqual('spiderman');
    });

    const req = httpTestingController.expectOne('http://localhost:3000/heroes/11');

    expect(req.request.method).toEqual('PUT');

    req.flush(mockHero);
  });

  it('Obtener todos los heroes', () => {
    const mockHero = {
      name: 'spiderman',
    };

    crudService.get().subscribe((heroes) => {
      expect(heroes.name).toEqual('spiderman');
    });

    const req = httpTestingController.expectOne('http://localhost:3000/heroes');

    expect(req.request.method).toEqual('GET');

    req.flush(mockHero);
  });

  it('Obtener heroe por id', () => {
    const mockHero = {
      name: 'spiderman',
      id: 10,
    };

    crudService.get(10).subscribe((heroes) => {
      expect(heroes.name).toEqual('spiderman');
    });

    const req = httpTestingController.expectOne('http://localhost:3000/heroes/10');

    expect(req.request.method).toEqual('GET');

    req.flush(mockHero);
  });

  it('Filtrar heroes por nombre', () => {
    const mockHeroes = [
      new Heroes(10, 'spiderman'),
      new Heroes(11, 'spiderman'),
      new Heroes(12, 'superman'),
    ];

    const heroes = crudService.filterHeroes(mockHeroes, 'spider');

    expect(heroes.length).toBe(2);
  });
});
