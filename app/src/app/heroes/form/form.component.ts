import { CrudService } from './../../core/crud-service.service';
import { LoadingService } from './../../loading/service/loading.service';
import { Heroes } from './../heroes';
import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  @Input() heroe: Heroes = new Heroes();
  @Output() heroeSaved: EventEmitter<boolean> = new EventEmitter();

  heroeForm = new FormGroup({
    name: new FormControl(this.heroe.name, [Validators.required, Validators.minLength(4)]),
  });

  constructor(
    private loadingService: LoadingService,
    private crudService: CrudService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.heroeForm.patchValue({
      name: this.heroe.name,
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  get name() {
    return this.heroeForm.get('name');
  }

  /**
   * Al enviar el formulario evalua si el héroe tiene id o no,
   * Si la tiene lanza la petición para actualizar el héroe, si no la tiene para crearlo.
   */
  onSubmit(): void {
    const heroeToSubmit = { name: this.heroeForm.controls['name'].value.toUpperCase() };

    if (this.heroe.id === -1) {
      this.crudService
        .create({ ...heroeToSubmit })
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => {
          this.heroeSaved.emit(true);
        });
    } else {
      this.crudService
        .update(this.heroe.id, { ...heroeToSubmit })
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => {
          this.heroeSaved.emit(true);
        });
    }
  }

  /**
   * Cierra el formulario y vuelve a mostrar la lista
   */
  cancelForm($event: any) {
    $event.preventDefault();
    this.heroeSaved.emit(false);
  }
}
