import { DialogComponent } from './dialog/dialog.component';
import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { CrudService } from '../core/crud-service.service';
import { Heroes } from './heroes';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss'],
})
export class HeroesComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  public heroes: Heroes[] = [];
  public filteredHeroes: Heroes[] = [];
  displayedColumns: string[] = ['checkbox', 'id', 'name'];
  public displayedHeroes: Heroes[] | undefined = undefined;
  public filterValue: string = '';
  public someHeroesChecked = false;
  public displayForm: boolean = true;
  public heroeToEdit: Heroes = new Heroes();
  public paginatorSize = 10;
  public paginatorIndex = 0;

  constructor(private crudService: CrudService, public dialog: MatDialog, cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.getHeroes();
  }

  /**
   *
   * Obtiene los heroes para mostrarlos en la lista
   */
  getHeroes($event?: any) {
    this.displayForm = false;

    if ($event === false) {
      return;
    }
    this.crudService
      .get()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data) => {
        this.heroes = data;
        this.filteredHeroes = data;
        this.setCheckedValues();
        this.updateDisplayedHeroes(this.paginatorSize, this.paginatorIndex);
      });
  }

  /**
   * Cancela todas las subscripciones
   */
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  /**
   * Detecta los cambios de dimensiones del paginador y llama a la funcion que actualiza los datos de muestra
   */
  handlePageEvent(event: PageEvent): void {
    this.paginatorSize = event.pageSize;
    this.paginatorIndex = event.pageIndex;
    this.updateDisplayedHeroes(event.pageSize, event.pageIndex);
  }

  /**
   * Actualiza los datos a mostrar dependiendo de la longitud de pagina del pagindor y del número de página
   */
  updateDisplayedHeroes(pageSize: number, pageIndex: number): void {
    this.displayedHeroes = this.filteredHeroes.slice(
      pageIndex * pageSize,
      (pageIndex + 1) * pageSize,
    );
  }

  /**
   * Añade la propiedad checked a todos los objetos del array por que no vienen con ella del backend
   */
  setCheckedValues() {
    this.heroes = this.heroes.map((heroe) => ({ ...heroe, checked: false }));
  }

  /**
   * Cuando se marca / desmarca un checkbox de la tabla setea todos los checked del array
   * para guardar el que esta marcado y busca si hay alguno marcado para guardarlo en someHeroesChecked
   */
  checkboxChanged(idHeroe: number): void {
    this.heroes.forEach((heroe) => {
      heroe.checked = heroe.id === idHeroe && !heroe.checked;
    });

    this.someHeroesChecked =
      this.heroes.find((heroe) => heroe.checked === true) === undefined ? false : true;
  }

  /**
   * Busca el héroe marcado
   */
  getCheckedHeroe(): Heroes | undefined {
    return this.heroes.find((heroe) => heroe.checked === true);
  }

  /**
   * Lanza el dialogo de confirmacion para eliminar un héroe,
   * si se confirma llama a la función que lanza la petición de eliminar
   */
  deleteSelectedHeroe(): void {
    if (!this.someHeroesChecked) {
      return;
    }

    const dialogRef = this.dialog.open(DialogComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.deleteHeroRequest();
      }
    });
  }

  /**
   * Lanza la petición para eliminar un héroe
   */
  deleteHeroRequest(): void {
    const selectedHeroe = this.getCheckedHeroe();

    if (selectedHeroe) {
      this.crudService
        .delete(selectedHeroe.id)
        .pipe(takeUntil(this.destroy$))
        .subscribe((data) => {
          this.getHeroes();
          this.resetChecked();
        });
    }
  }

  /**
   * Llama al servicio para filtrar los heroes y actualiza los heroes mostrados en la tabla
   * para que se vean los filtrados
   */
  filterHeroes($event: any) {
    this.filteredHeroes = this.crudService.filterHeroes(this.heroes, $event.target.value);
    this.updateDisplayedHeroes(10, 0);
  }

  /**
   * Lanza la petición para obtener el héroe seleccionado por si se han modificado sus datos
   * y lo muestra en el formulario
   */
  editHeroe(): void {
    if (!this.someHeroesChecked) {
      return;
    }

    const selectedHeroe = this.getCheckedHeroe();

    if (selectedHeroe) {
      this.crudService
        .get(selectedHeroe.id)
        .pipe(takeUntil(this.destroy$))
        .subscribe((data) => {
          this.heroeToEdit = data;
          this.displayForm = true;
          this.resetChecked();
        });
    }
  }

  /**
   * Muestra el formulario para crear un nuevo héroe pero antes resetea el héroe a editar por
   * si se ha seteado anteriormente
   */
  newHeroe() {
    this.heroeToEdit = new Heroes();
    this.showForm();
  }

  /**
   * Muestra el formulario de creacion / edición
   */
  showForm(): void {
    this.resetChecked();
    this.displayForm = true;
  }

  /**
   * Desmarca todos los héroes seleccionados
   */
  resetChecked() {
    this.setCheckedValues();
    this.someHeroesChecked = false;
  }
}
