import { CrudService } from './../core/crud-service.service';
import { SharedModule } from './../shared.module';
import { MatDialogModule } from '@angular/material/dialog';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { HeroesComponent } from './heroes.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { Heroes } from './heroes';
import { of } from 'rxjs';

describe('HeroesComponent', () => {
  let component: HeroesComponent;
  let fixture: ComponentFixture<HeroesComponent>;
  let crudService: CrudService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeroesComponent],
      imports: [
        HttpClientModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatDialogModule,
        SharedModule,
      ],
      providers: [CrudService],
    }).compileComponents();

    crudService = TestBed.inject(CrudService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Añade checked a false a todos los héroes', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;

    const mockHeroes = [{ name: 'spiderman' }, { name: 'superman' }];
    componentInstance.heroes = mockHeroes;
    componentInstance.setCheckedValues();

    componentInstance.heroes.forEach((heroe: Heroes) => {
      expect(heroe.checked).toBe(false);
    });
  });

  it('Marca y desmarca correctamente cada heroe y detecta si hay uno seleccionado', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;

    const mockHeroes = [
      { id: 1, name: 'spiderman' },
      { id: 2, name: 'superman' },
    ];
    componentInstance.heroes = mockHeroes;
    componentInstance.checkboxChanged(1);
    expect(componentInstance.heroes[0].checked).toBe(true);
    expect(componentInstance.someHeroesChecked).toBe(true);
  });

  it('Devuelve correctamente el heroe seleccionado', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;

    const mockHeroes = [
      { id: 1, name: 'spiderman' },
      { id: 2, name: 'superman' },
    ];
    componentInstance.heroes = mockHeroes;
    componentInstance.checkboxChanged(1);
    const checkedHeroe = componentInstance.getCheckedHeroe();
    expect(checkedHeroe.name).toBe('spiderman');
  });

  it('Actualiza correctamente el numero de heroes a mostrar', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;

    const mockHeroes = [
      { id: 1, name: 'spiderman' },
      { id: 2, name: 'superman' },
      { id: 3, name: 'superman' },
    ];
    componentInstance.filteredHeroes = mockHeroes;
    componentInstance.updateDisplayedHeroes(2, 0);

    expect(componentInstance.displayedHeroes.length).toBe(2);
  });

  it('Filtra los héroes correctamente', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;

    const mockHeroes = [
      { id: 1, name: 'spiderman' },
      { id: 2, name: 'superman' },
      { id: 3, name: 'superman' },
    ];
    componentInstance.heroes = mockHeroes;
    const event = { target: { value: 'spider' } };
    componentInstance.filterHeroes(event);

    expect(componentInstance.displayedHeroes.length).toBe(1);
  });

  it('El formulario se muestra correctamente', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;
    spyOn(componentInstance, 'resetChecked');
    componentInstance.showForm();
    expect(componentInstance.resetChecked).toHaveBeenCalled();
    expect(componentInstance.displayForm).toBe(true);
  });

  it('Resetea los heroes marcados', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;
    const mockHeroes = [{ name: 'spiderman' }, { name: 'superman' }];
    componentInstance.heroes = mockHeroes;

    spyOn(componentInstance, 'setCheckedValues');
    componentInstance.resetChecked();
    expect(componentInstance.setCheckedValues).toHaveBeenCalled();
    expect(componentInstance.someHeroesChecked).toBe(false);
  });

  it('La peticion para obtener los heroes se lanza y recibe', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;

    spyOn(crudService, 'get').and.returnValue(of([{ name: 'spiderman' }]));

    spyOn(componentInstance, 'setCheckedValues');
    spyOn(componentInstance, 'updateDisplayedHeroes');

    componentInstance.getHeroes();

    fixture.detectChanges();

    expect(componentInstance.heroes).toEqual([{ name: 'spiderman' }]);
    expect(componentInstance.filteredHeroes).toEqual([{ name: 'spiderman' }]);
    expect(componentInstance.setCheckedValues).toHaveBeenCalled();
    expect(componentInstance.updateDisplayedHeroes).toHaveBeenCalledWith(10, 0);
  });

  it('La peticion para eliminar los heroes se lanza y recibe', () => {
    const fixture = TestBed.createComponent(HeroesComponent);
    const componentInstance = fixture.debugElement.componentInstance;
    spyOn(crudService, 'delete').and.returnValue(of([{ name: 'spiderman' }]));
    spyOn(componentInstance, 'getCheckedHeroe').and.returnValue(
      of({ name: 'superman', checked: true, id: 2 }),
    );

    spyOn(componentInstance, 'resetChecked');
    spyOn(componentInstance, 'getHeroes');

    console.log(componentInstance.getCheckedHeroe());
    componentInstance.deleteHeroRequest();

    fixture.detectChanges();

    expect(componentInstance.getCheckedHeroe).toHaveBeenCalled();
    expect(componentInstance.resetChecked).toHaveBeenCalled();
    expect(componentInstance.getHeroes).toHaveBeenCalled();
  });
});
