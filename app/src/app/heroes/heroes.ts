export class Heroes {
  public id: number;
  public name: string;
  public checked: boolean;

  constructor(id?: number, name?: string, checked?: boolean) {
    this.id = id || -1;
    this.name = name || '';
    this.checked = checked || false;
  }
}
