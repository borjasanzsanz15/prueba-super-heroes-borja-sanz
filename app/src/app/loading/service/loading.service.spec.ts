import { TestBed } from '@angular/core/testing';

import { LoadingService } from './loading.service';

describe('LoadingService', () => {
  let loadingService: LoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    loadingService = TestBed.inject(LoadingService);
  });

  it('should be created', () => {
    expect(loadingService).toBeTruthy();
  });


  it('Añade nueva url al array de carga', () => {
    loadingService.setLoadingPage('http://example.com');

    expect(loadingService.loading).toBeTruthy();
    expect(loadingService.currentLoadingPages).toEqual(['http://example.com']);
  });


  it('Elimina una url del array de carga', () => {


    loadingService.loading = true;
    loadingService.currentLoadingPages = ['http://example.com'];

    loadingService.endLoadingPage('http://example.com');

    expect(loadingService.loading).toBeFalsy();
    expect(loadingService.currentLoadingPages).toEqual([]);
  });
});
