import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  public loading: boolean = false;
  public currentLoadingPages: string[] = [];

  constructor() { }

  /**
   * Guarda en el array de peticiones que estan en marcha la url recibida
   * y cambia el valor de loading, a true si hay alguna url en el array y si no a false
   */
  setLoadingPage(url: string) {
    this.currentLoadingPages?.push(url);
    this.loading = this.currentLoadingPages.length > 0;
  }

  /**
   * Elimina en el array de peticiones que estan en marcha la url recibida
   * y cambia el valor de loading, a true si hay alguna url en el array y si no a false
   */
  endLoadingPage(url: string) {
    this.currentLoadingPages.forEach((loadingPage, index) => {
      if (loadingPage === url) this.currentLoadingPages.splice(index, 1);
    });

    this.loading = this.currentLoadingPages.length > 0;
  }
}
