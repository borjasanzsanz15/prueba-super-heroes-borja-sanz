Pasos para iniciar la aplicación:

1.- Hacer el build de la imagen de docker:

- docker build -t node_nginx -f Dockerfile .

2.- Ejecutar la imagen con docker compose o con docker directamente:

- docker compose: docker-compose up -d

- docker: docker run -d -p 80:80 node_image

Podremos acceder a la aplicación en http://localhost

3. Iniciar servidor con json de prueba:

- npx json-server --watch mock-heroes.json -d 1000
