FROM node:14-alpine AS build
WORKDIR /app
COPY /app ./
COPY /app/package*.json ./

RUN npm install -g @angular/cli@12.1.1 && \
    npm install && \
    ng build


FROM nginx:1.21.1-alpine
WORKDIR /app
COPY --from=build /app/dist/heroes-borja-sanz /usr/share/nginx/html